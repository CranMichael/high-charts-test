﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HighChartsTest.Models
{
    public class NcByArea
    {
        public string name { get; set; }
        public List<string[]> data { get; set; }
    }

    public class NcDetails
    {
        public string Date { get; set; }
        public int Value { get; set; }
    }
}