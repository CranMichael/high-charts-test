﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HighChartsTest.Models;
using LiveHACCP.Dashboard.Reports;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace HighChartsTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HighChartTest()
        {

            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GetConcessionsAwaitingResponse(DateTime startDate, DateTime endDate)
        {
            var jsonString =  new Reports(Properties.Settings.Default.ConnString, Properties.Settings.Default.CompanyId, Properties.Settings.Default.FactoryId).JsonConcessionsAwaitingResponse(startDate, endDate);
            
            JObject json = JObject.Parse(jsonString);
            return this.Content(jsonString, "application/json");
        }

        public ActionResult GetCriticalNonConformances(DateTime startDate, DateTime endDate)
        {
            var jsonString =  new Reports(Properties.Settings.Default.ConnString, Properties.Settings.Default.CompanyId, Properties.Settings.Default.FactoryId).JsonCriticalNonConformances(startDate, endDate);
            return this.Content(jsonString, "application/json");
        }

        public ActionResult GetFormsCompletedByArea(DateTime startDate, DateTime endDate)
        {
            var jsonString =  new Reports(Properties.Settings.Default.ConnString, Properties.Settings.Default.CompanyId, Properties.Settings.Default.FactoryId).JsonFormsCompletedByArea(startDate, endDate);
            return this.Content(jsonString, "application/json");
        }
        
        public ActionResult GetIncidentsByArea(DateTime startDate, DateTime endDate)
        {
            var jsonString =  new Reports(Properties.Settings.Default.ConnString, Properties.Settings.Default.CompanyId, Properties.Settings.Default.FactoryId).JsonIncidentsByArea(startDate, endDate);
            return this.Content(jsonString, "application/json");
        }

        public ActionResult GetNcResponseTime(DateTime startDate, DateTime endDate)
        {
            var jsonString =  new Reports(Properties.Settings.Default.ConnString, Properties.Settings.Default.CompanyId, Properties.Settings.Default.FactoryId).JsonNcResponseTime(startDate, endDate);
            return this.Content(jsonString, "application/json");
        }

        public ActionResult GetNonConformanceByArea(DateTime startDate, DateTime endDate)
        {
            var jsonString = new Reports(Properties.Settings.Default.ConnString, Properties.Settings.Default.CompanyId, Properties.Settings.Default.FactoryId).JsonNonConformanceByArea(startDate, endDate);

            dynamic dynJson = JsonConvert.DeserializeObject(jsonString);

            List<NcByArea> ncByAreas = new List<NcByArea>();

            foreach (var j in dynJson)
            {
                if (ncByAreas.Find(a=>a.name == j.AreaName.ToString()) != null) continue;

                NcByArea nc = new NcByArea()
                {
                    name = j.AreaName,
                    data = new List<string[]>()
                };
                ncByAreas.Add(nc);
            }
            
            foreach (var j in dynJson)
            {
                DateTime date = Convert.ToDateTime(j.Date.ToString());
                string[] details2 = new string[]
                {
                    GetMillisecondDateFormat(date),
                    j.TotalNC.ToString()
                };

                ncByAreas.Find(a=>a.name == j.AreaName.ToString()).data
                    .Add(details2);
            }


            var json2 = JsonConvert.SerializeObject(ncByAreas, Formatting.Indented);

            return this.Content(json2, "application/json");
        }

        private string GetMillisecondDateFormat(DateTime date)
        {
            var jsStartDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);

            return date.ToUniversalTime()
                .Subtract(jsStartDate).Add(new TimeSpan(0, 1, 0, 0))
                .TotalMilliseconds.ToString();
        }

        public ActionResult GetPlannedMaintenanceCompleted(DateTime startDate, DateTime endDate)
        {
            var jsonString =  new Reports(Properties.Settings.Default.ConnString, Properties.Settings.Default.CompanyId, Properties.Settings.Default.FactoryId).JsonPlannedMaintenanceCompleted(startDate, endDate);
            return this.Content(jsonString, "application/json");
        }

        public ActionResult GetUnplannedMaintenanceCompleted(DateTime startDate, DateTime endDate)
        {
            var jsonString =  new Reports(Properties.Settings.Default.ConnString, Properties.Settings.Default.CompanyId, Properties.Settings.Default.FactoryId).JsonUnplannedMaintenanceCompleted(startDate, endDate);
            return this.Content(jsonString, "application/json");
        }
    }
}